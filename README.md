# Dzahin Shop Use Case Backend Service

**Dzahin Shop an Use Case Project of Softdev Telkomsel**

This repository contains backend codes of Service Dzahin Shop. All codes written in Java.
This service can 
1. Display product with ability to search, pagination, and display promo each product
2. Add, delete, update to cart multiple product
3. Checkout selected product on cart
4. Identifier by email simply to distinct each user, no authorization & registration
5. Calculate total price, discount, bonus at cart & order service
6. This service still can’t proceed to order, payment, user membership [ To Do & Improvement Part ] 

**Technologies**

All of our services are in REST API and build on top of these technologies:

1. SpringBoot as framework for backend services
2. Spring Data JPA, as framework for ORM and persistent layer
3. jUnit5 & Mockito for Testing purpose
4. Docker as containerized for deployment
5. Gitlab CI-CD to run pipelining start form BUILD → TEST → DOCKERIZED → DEPLOY

**How do I get set up ?**

1. Clone this repo
2. Install java & open in intelijIdea / eclipse
3. Setting environment variable for database connection 
    * DB_SERVER : your database server
    * DB_PWD : your database password
or you can change connection string at /resources/application.properties
4. Reload maven project to automatically download dependency
5. Start Coding

**How to check or Test this app**

1. Read all complete documentation API request & response at 
    * Direct link : [https://shop.dzahin.com/docs/](https://shop.dzahin.com/docs/)
    * Download docs.html at [here](https://gitlab.com/agusdwi89/dzahin-shop-use-case/-/raw/main/public/postman.docs.html?inline=false)
    * Postman Collection download at [here](https://gitlab.com/agusdwi89/dzahin-shop-use-case/-/raw/main/public/postman.collection.json?inline=false) 
2. Trial directly 
    * Endpoint : > https://shop.dzahin.com/api/product?page=1&size=10
    * x-api-key : 12345678 ( please wisely using this, free tier cloud provided by oracle 😀 )
3. Run on your own machine by pulling docker image
    * download docker-compose.xml at [here](https://gitlab.com/agusdwi89/dzahin-shop-use-case/-/raw/main/public/docker-compose.yml?inline=false)
    * configure the database connection at docker-compose.xml 
    * run docker-compose up -d
4. Clone repo and run manually from IDE intelJ or Eclipse
5. For Unit Testing run **mvn test**

**CI CD Pipeline**

![Text](https://gitlab.com/agusdwi89/dzahin-shop-use-case/-/raw/main/public/img.png)

This repo has implemented the ci cd pipeline to simplify testing & deployment to env development

below is the job that will run once there is a change in the code

1. **Maven Build** : Test build the whole code
2. **Testing** : a unit test provided by junit, with total at least 15 test cases, including Logic discount calculator, validation request response & checking server status
3. **Dockerize** : Build docker container ready to deploy
4. **Deploy-dev** : deploy to development server, in this case jobs deploy to oracle cloud free tier & server credential saved to secret variable


***Happy Coding***
agusprg
