package com.dzahin.dzahinshop.service;

import com.dzahin.dzahinshop.helper.BusinessException;
import com.dzahin.dzahinshop.model.entity.Cart;
import com.dzahin.dzahinshop.model.entity.Merchant;
import com.dzahin.dzahinshop.model.entity.TokenUser;
import com.dzahin.dzahinshop.model.request.AddCartRequest;
import com.dzahin.dzahinshop.model.request.CheckoutRequest;
import com.dzahin.dzahinshop.repository.CartRepository;
import com.dzahin.dzahinshop.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;

import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.*;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AddToCartServiceImpl implements AddToCartService{

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;

    @Override
    public Map<String, Object> getCartData(Integer userId) {

        List<Cart> cartItem = cartRepository.findAllByUserId(userId);
        return getHeaderCart(cartItem);
    }

    @Transactional
    @Override
    public void addToCart(Integer userId, AddCartRequest userCart) {
        List<Cart> cart = cartRepository.findAllByUserId(userId), cartFinal = new ArrayList<>();
        HashMap<Integer,Cart> maps = new HashMap<>();

        productRepository.findById(userCart.getProductId()).orElseThrow(BusinessException::productNotFound);

        if(!cart.isEmpty())
            cart.forEach(cart1 -> {
                maps.put(cart1.getProductId(),cart1);
            });
        if(maps.containsKey(userCart.getProductId())){
            Cart tempCart = maps.get(userCart.getProductId());
            tempCart.setQty(tempCart.getQty()+ userCart.getQty());
            maps.put(userCart.getProductId(),tempCart);
        }else{
            maps.put(userCart.getProductId(),
                    Cart.builder()
                            .productId(userCart.getProductId())
                            .qty(userCart.getQty())
                            .userId(userId)
                            .build());
        }

        maps.forEach((integer, cart1) -> cartFinal.add(cart1));
        cartRepository.saveAll(cartFinal);
    }

    @Transactional
    @Override
    public void updateToCart(Integer userId, AddCartRequest userCart) {
        Cart tempCart = cartRepository.findByProductIdAndUserId(userCart.getProductId(),userId)
                .orElseThrow(BusinessException::productNotFound);
        if(userCart.getQty() == 0)
            cartRepository.deleteById(tempCart.getId());
        else{
            tempCart.setQty(userCart.getQty());
            cartRepository.save(tempCart);
        }
    }

    @Transactional
    @Override
    public void deleteCart(Integer userId, Integer productId) {
        Cart cart = cartRepository.findByProductIdAndUserId(productId,userId)
                .orElseThrow(BusinessException::productNotFound);
        cartRepository.delete(cart);
    }

    private static Map<String, Object> getHeaderCart(List<Cart> cartItem){
        Map<String,Object> CartContent = new HashMap<>();
        Map<String,Object> cartHeader = new HashMap<>();

        Integer defaultPrice,actualPrice,discountedPrice,totalItem,totalProduct;
        defaultPrice=actualPrice=discountedPrice=totalItem=totalProduct=0;
        for (Cart res: cartItem){
            defaultPrice += res.getPriceCalculate().getDefaultPrice();
            actualPrice += res.getPriceCalculate().getActualPrice();
            discountedPrice += res.getPriceCalculate().getDiscountedPrice();
            totalItem += res.getQty();
        }
        totalProduct = cartItem.size();

        cartHeader.put("defaultPrice",defaultPrice);
        cartHeader.put("actualPrice",actualPrice);
        cartHeader.put("discountedPrice",discountedPrice);
        cartHeader.put("totalItem",totalItem);
        cartHeader.put("totalProduct",totalProduct);

        CartContent.put("cartHeader",cartHeader);
        CartContent.put("cartItem",getCartGrouping(cartItem));

        return CartContent;
    }

    private static List<Object> getCartGrouping(List<Cart> cartItem){
        Map<Integer,List<Cart>> cartGroup = new HashMap<>();
        Map<Integer, Merchant> merchantGroup = new HashMap<>();
        cartItem.forEach(cart -> {
            Integer merchantId = cart.getProduct().getMerchantId();
            String name = cart.getProduct().getMerchant().getName();
            merchantGroup.put(merchantId,new Merchant(merchantId,name));
            List<Cart> tempCart = new ArrayList<>();
            if(cartGroup.containsKey(merchantId)){
                tempCart = cartGroup.get(merchantId);
            }
            tempCart.add(cart);
            cartGroup.put(merchantId,tempCart);
        });
        List<Object> ll = new ArrayList<>();
        for (Integer key : merchantGroup.keySet()) {
            List<Cart> cartId = cartGroup.get(key);
            Merchant merchant = merchantGroup.get(key);
            Map<String,Object> summary = new HashMap<>();
            summary.put("merchant",merchant);
            summary.put("items",cartId);
            ll.add(summary);
        }
        return ll;
    }

    @Override
    public Map<String, Object> checkout(CheckoutRequest checkoutRequest, TokenUser t) {

        if(checkoutRequest.getCartId().isEmpty())
            throw BusinessException.productNotFound();

        List<Cart> checkoutItem = cartRepository.findIdsandUserId(checkoutRequest.getCartId(),t.getId());
        HashSet<Integer> numMerchant = new HashSet<>();
        checkoutItem.forEach(cart -> {
            numMerchant.add(cart.getProduct().getMerchantId());
            if(cart.getProduct().getStock() < cart.getQty())
                throw BusinessException.outOfStock(cart.getProduct().getName());
        });
        if(numMerchant.size()>1)
            throw BusinessException.multipleMerchantCheckout();

        if(checkoutItem.size() != checkoutRequest.getCartId().size())
            throw BusinessException.productNotFound();

        return this.getHeaderCart(checkoutItem);
    }

    @Transactional
    @Override
    public void checkoutOrder(CheckoutRequest checkoutRequest, TokenUser t) {

        /***
        * TO DO NEXT: order processing request
        ***/

        if(checkoutRequest.getCartId().isEmpty())
            throw BusinessException.productNotFound();

        List<Cart> checkoutItem = cartRepository.findIdsandUserId(checkoutRequest.getCartId(),t.getId());
        if(checkoutItem.size() != checkoutRequest.getCartId().size())
            throw BusinessException.productNotFound();

        HashSet<Integer> numMerchant = new HashSet<>();
        checkoutItem.forEach(cart -> {
            numMerchant.add(cart.getProduct().getMerchantId());
            if(cart.getProduct().getStock() < cart.getQty())
                throw BusinessException.outOfStock(cart.getProduct().getName());
        });
        if(numMerchant.size()>1)
            throw BusinessException.multipleMerchantCheckout();
        checkoutItem.forEach(cart -> {
            productRepository.updateStock(cart.getProductId(), (cart.getProduct().getStock() - cart.getQty()));
        });
        cartRepository.deleteAllById(checkoutRequest.getCartId());
    }
}