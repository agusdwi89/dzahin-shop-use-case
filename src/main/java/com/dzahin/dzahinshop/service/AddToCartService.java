package com.dzahin.dzahinshop.service;

import com.dzahin.dzahinshop.model.entity.TokenUser;
import com.dzahin.dzahinshop.model.request.AddCartRequest;
import com.dzahin.dzahinshop.model.request.CheckoutRequest;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface AddToCartService {
    void addToCart(Integer userId, AddCartRequest userCart);
    void updateToCart(Integer userId, AddCartRequest userCart);
    void deleteCart(Integer userId, Integer productId);
    Map<String, Object> getCartData(Integer id);
    Map<String, Object> checkout(CheckoutRequest checkoutRequest, TokenUser t);
    void checkoutOrder(CheckoutRequest checkoutRequest, TokenUser t);
}
