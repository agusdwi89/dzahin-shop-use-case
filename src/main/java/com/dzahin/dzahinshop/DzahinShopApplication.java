package com.dzahin.dzahinshop;

import com.dzahin.dzahinshop.helper.BaseConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SpringBootApplication
public class DzahinShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(DzahinShopApplication.class, args);
	}
	Logger LOGGER = LogManager.getLogger(DzahinShopApplication.class);

	@Component
	public class ApiKeyRequestFilter extends GenericFilterBean {
		@Override
		public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
			HttpServletRequest req = (HttpServletRequest) request;
			String path = req.getRequestURI();
			if(path.startsWith("/api") == false){
				chain.doFilter(request, response);
				return;
			}
			String key = req.getHeader("x-api-key") == null ? "" : req.getHeader("x-api-key");
			LOGGER.info("Trying key: " + key);
			if(key.equals(BaseConstants.API_KEY)){
				chain.doFilter(request, response);
			}else{
				HttpServletResponse resp = (HttpServletResponse) response;
				String error = "Invalid API KEY";
				resp.reset();
				resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.setContentLength(error .length());
				response.getWriter().write(error);
			}
		}
	}
}
