package com.dzahin.dzahinshop.repository;

import com.dzahin.dzahinshop.model.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {

    Page<Product> findAll(Pageable pageable);
    Page<Product> findByNameContaining(String key, Pageable pageable);

    @Modifying
    @Query("update Product p set p.stock = :stock where p.id = :id")
    void updateStock(Integer id, int stock);
}
