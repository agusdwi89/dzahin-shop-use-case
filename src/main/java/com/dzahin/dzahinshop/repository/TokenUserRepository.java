package com.dzahin.dzahinshop.repository;

import com.dzahin.dzahinshop.model.entity.TokenUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface TokenUserRepository extends JpaRepository<TokenUser,Integer> {

    Optional<TokenUser> findOneByEmail(String email);
    Optional<TokenUser> findOneByToken(String token);
}
