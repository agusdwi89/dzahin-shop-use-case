package com.dzahin.dzahinshop.repository;

import com.dzahin.dzahinshop.model.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface    CartRepository extends JpaRepository<Cart, Integer> {

    List<Cart> findAllByUserId(Integer userId);
    Optional<Cart> findByProductIdAndUserId(Integer productId, Integer userId);

    @Query("SELECT c FROM Cart c WHERE c.id in :cartId and c.userId = :userId")
    List<Cart> findIdsandUserId(@Param("cartId") List<Integer> cartId, @Param("userId") Integer userId);
}
