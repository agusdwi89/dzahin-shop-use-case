package com.dzahin.dzahinshop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@Entity
@Table(name="TEST")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestModel {

    @Id
    @GeneratedValue
    @JsonIgnore
    private Integer id;

    private String name;
}
