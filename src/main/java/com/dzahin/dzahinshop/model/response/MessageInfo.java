package com.dzahin.dzahinshop.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageInfo {
    private String code;
    private String title;
    private String message;
    private List<String> errorList;
}
