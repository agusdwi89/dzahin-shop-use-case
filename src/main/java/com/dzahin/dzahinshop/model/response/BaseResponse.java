package com.dzahin.dzahinshop.model.response;

import com.dzahin.dzahinshop.helper.BusinessException;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse<T> {
    private Integer status;
    private String message;
    private T data;
    private MessageInfo messageInfo;
    private Object exception;
    private static final String WARNING = "PERHATIAN";

    public static BaseResponse createSuccessResponse() {
        BaseResponse response = new BaseResponse();
        response.setStatus(0);
        response.setMessage("ok");
        return response;
    }

    public static BaseResponse createSuccessResponse(Object data) {
        BaseResponse response = createSuccessResponse();
        response.setData(data);
        return response;
    }


    public static BaseResponse createFailedResponse(Integer statusCode) {
        BaseResponse response = new BaseResponse();
        response.setStatus(1);
        MessageInfo info = new MessageInfo();
        info.setCode(statusCode.toString());
        info.setMessage("Terjadi kesalahan pada system");
        info.setTitle(WARNING);
        response.setMessageInfo(info);
        return response;
    }

    public static BaseResponse createFailedResponse(Integer statusCode,String message) {
        BaseResponse response = new BaseResponse();
        response.setStatus(1);
        MessageInfo info = new MessageInfo();
        info.setCode(statusCode.toString());
        info.setMessage(message);
        info.setTitle(WARNING);
        response.setMessageInfo(info);
        return response;
    }

    public static BaseResponse createFailedResponse(Integer statusCode, List<String> errorList) {
        BaseResponse response = new BaseResponse();
        response.setStatus(1);
        MessageInfo info = new MessageInfo();
        info.setCode(statusCode.toString());
        info.setTitle(WARNING);
        info.setErrorList(errorList);
        response.setMessageInfo(info);
        return response;
    }

    public static BaseResponse createFailedResponse(BusinessException ex) {
        BaseResponse response = new BaseResponse();
        response.setStatus(1);
        MessageInfo info = new MessageInfo();
        info.setCode(ex.getErrorCode());
        info.setMessage(ex.getMessage());
        info.setTitle(WARNING);
        response.setMessageInfo(info);
        return response;
    }
}
