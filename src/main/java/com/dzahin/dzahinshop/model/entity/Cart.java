package com.dzahin.dzahinshop.model.entity;

import com.dzahin.dzahinshop.helper.DiscountCalculator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name="CART_ITEM")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Cart implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @JsonIgnore
    private Integer userId;
    private Integer qty;
    @JsonIgnore
    private Integer productId;

    @OneToOne
    @JoinColumn(name = "productId", referencedColumnName = "id",insertable=false, updatable=false)
    private Product product;

    @Transient
    PriceCalculate priceCalculate = null;

    public PriceCalculate getPriceCalculate() {
        return DiscountCalculator.calculatePriceProduct(this.product,this.qty);
    }

}