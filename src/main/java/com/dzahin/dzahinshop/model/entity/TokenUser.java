package com.dzahin.dzahinshop.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Data
@Entity
@Table(name="TOKEN_USER")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TokenUser implements Serializable {

    @Id @GeneratedValue(strategy= GenerationType.AUTO)
    @JsonIgnore
    private Integer id;

    private String token;

    private String email;

    public TokenUser(String randomUUID, String email) {
        this.token = randomUUID;
        this.email = email;
    }
}
