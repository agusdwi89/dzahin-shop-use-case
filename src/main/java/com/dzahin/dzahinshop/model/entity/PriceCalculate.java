package com.dzahin.dzahinshop.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PriceCalculate {

    private Integer defaultPrice = 0;
    private Integer actualPrice = 0;
    private Integer discountedPrice = 0;
    private Integer actualQty = 0;
    private Integer freeQty = 0;
}
