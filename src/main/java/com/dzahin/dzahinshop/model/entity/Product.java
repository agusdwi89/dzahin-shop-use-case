package com.dzahin.dzahinshop.model.entity;

import com.dzahin.dzahinshop.helper.DiscountCalculator;
import com.dzahin.dzahinshop.repository.ProductRepository;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Data
@Entity
@Table(name="PRODUCT")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String name;
    private Integer price;

    @Min(value=1)
    private Integer buyCondition;
    @Min(value=1)
    private Integer freeBonus;
    private Integer stock;

    @JsonIgnore
    private Integer merchantId = null;

    @OneToOne
    @JoinColumn(name = "merchantId", referencedColumnName = "id",insertable=false, updatable=false)
    private Merchant merchant;

    @Transient
    private String promo = null;

    public String getPromo() {
        if(this.freeBonus != null && this.buyCondition != null){
            if(this.freeBonus > 0 & this.buyCondition > 0){
                return "buy " + this.buyCondition + " Get " + this.freeBonus + " free";
            }
        }
        return "";
    }
}
