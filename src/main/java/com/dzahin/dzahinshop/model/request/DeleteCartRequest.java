package com.dzahin.dzahinshop.model.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Data
public class DeleteCartRequest {

    @NotNull(message = "id product tidak boleh kosong")
    private Integer productId;
}
