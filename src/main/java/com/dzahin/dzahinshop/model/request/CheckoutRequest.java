package com.dzahin.dzahinshop.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;


@Data
public class CheckoutRequest {
    @NotNull(message = "list cart id tidak boleh kosong | {'cartId':[1,2]}")
    private List<Integer> cartId;
}
