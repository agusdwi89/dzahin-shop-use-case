package com.dzahin.dzahinshop.model.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Data
public class AddCartRequest {

    @NotNull(message = "id product tidak boleh kosong")
    private Integer productId;

    @NotNull(message = "qty tidak boleh kosong")
    @Min(value=0,message="qty harus >= 1")
    private Integer qty;
}
