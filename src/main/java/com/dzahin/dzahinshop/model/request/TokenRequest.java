package com.dzahin.dzahinshop.model.request;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class TokenRequest {

    @NotNull(message = "email tidak boleh kosong")
    @NotBlank(message = "email tidak boleh kosong")
    @Email(message = "format email salah")
    private String email;
}
