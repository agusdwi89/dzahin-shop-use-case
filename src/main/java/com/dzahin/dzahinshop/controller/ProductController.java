package com.dzahin.dzahinshop.controller;

import com.dzahin.dzahinshop.helper.BusinessException;
import com.dzahin.dzahinshop.model.entity.Product;
import com.dzahin.dzahinshop.model.response.BaseResponse;
import com.dzahin.dzahinshop.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Validated
@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProductController {

    private final static Logger LOGGER = LogManager.getLogger(TokenController.class);
    private final ProductRepository productRepository;

    @GetMapping("")
    BaseResponse getProductData(
            @RequestParam Integer page,
            @RequestParam @Min(0) @Max(10) Integer size ){
        Pageable pageable = PageRequest.of(page, size);
        Page<Product> productList = productRepository.findAll(pageable);
        return BaseResponse.createSuccessResponse(productList);
    }

    @GetMapping("search")
    BaseResponse getProductDataSearch(
            @RequestParam String key,
            @RequestParam Integer page,
            @RequestParam @Min(0) @Max(10) Integer size){
        Pageable pageable = PageRequest.of(page, size);
        Page<Product> productList = productRepository.findByNameContaining(key,pageable);
        return BaseResponse.createSuccessResponse(productList);
    }

}