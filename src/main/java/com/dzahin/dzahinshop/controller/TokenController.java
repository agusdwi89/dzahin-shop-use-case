package com.dzahin.dzahinshop.controller;

import com.dzahin.dzahinshop.helper.BusinessException;
import com.dzahin.dzahinshop.model.entity.TokenUser;
import com.dzahin.dzahinshop.model.request.TokenRequest;
import com.dzahin.dzahinshop.model.response.BaseResponse;
import com.dzahin.dzahinshop.repository.TestRepository;
import com.dzahin.dzahinshop.repository.TokenUserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.UUID;


@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TokenController {

    private final static Logger LOGGER = LogManager.getLogger(TokenController.class);
    private final TokenUserRepository tokenUserRepository;

    @Transactional
    @PostMapping("/api/token")
    public BaseResponse setToken(@Valid @RequestBody TokenRequest tokenRequest) {
        TokenUser t = tokenUserRepository.findOneByEmail(tokenRequest.getEmail())
                .orElse(
                    new TokenUser(UUID.randomUUID().toString(),tokenRequest.getEmail())
                );
        tokenUserRepository.save(t);
        return BaseResponse.createSuccessResponse(t);
    }
}
