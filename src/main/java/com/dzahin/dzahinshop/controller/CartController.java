package com.dzahin.dzahinshop.controller;

import com.dzahin.dzahinshop.helper.BusinessException;
import com.dzahin.dzahinshop.model.entity.TokenUser;
import com.dzahin.dzahinshop.model.request.AddCartRequest;
import com.dzahin.dzahinshop.model.request.CheckoutRequest;
import com.dzahin.dzahinshop.model.request.DeleteCartRequest;
import com.dzahin.dzahinshop.model.response.BaseResponse;
import com.dzahin.dzahinshop.repository.TokenUserRepository;
import com.dzahin.dzahinshop.service.AddToCartServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/cart")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CartController {

    private final static Logger LOGGER = LogManager.getLogger(TokenController.class);
    private final TokenUserRepository tokenUserRepository;
    private final AddToCartServiceImpl addToCartService;

    @GetMapping("")
    BaseResponse getCartData(@Valid @RequestHeader("token") String token){

        TokenUser t = tokenUserRepository.findOneByToken(token).orElseThrow(BusinessException::unauthorized);
        return BaseResponse.createSuccessResponse(
                addToCartService.getCartData(t.getId())
        );
    }

    @PostMapping("")
    BaseResponse addToCart(@Valid @RequestBody AddCartRequest userCart, @Valid @RequestHeader("token") String token){
        TokenUser t = tokenUserRepository.findOneByToken(token).orElseThrow(BusinessException::unauthorized);
        addToCartService.addToCart(t.getId(),userCart);
        return BaseResponse.createSuccessResponse();
    }

    @PutMapping("")
    BaseResponse updateCart(@Valid @RequestBody AddCartRequest userCart, @Valid @RequestHeader("token") String token){
        TokenUser t = tokenUserRepository.findOneByToken(token).orElseThrow(BusinessException::unauthorized);
        addToCartService.updateToCart(t.getId(),userCart);
        return BaseResponse.createSuccessResponse();
    }

    @DeleteMapping("")
    BaseResponse deleteCart(@Valid @RequestBody DeleteCartRequest userCart, @Valid @RequestHeader("token") String token){
        TokenUser t = tokenUserRepository.findOneByToken(token).orElseThrow(BusinessException::unauthorized);
        addToCartService.deleteCart(t.getId(),userCart.getProductId());
        return BaseResponse.createSuccessResponse();
    }

    @PostMapping("/checkout")
    BaseResponse processCheckout(
            @Valid @RequestBody CheckoutRequest checkoutRequest,
            @Valid @RequestHeader("token") String token){

        TokenUser t = tokenUserRepository.findOneByToken(token).orElseThrow(BusinessException::unauthorized);
        Map<String, Object> a = addToCartService.checkout(checkoutRequest,t);
        return BaseResponse.createSuccessResponse(a);
    }

    @PostMapping("/checkout/order")
    BaseResponse processCheckoutOrder(
            @Valid @RequestBody CheckoutRequest checkoutRequest,
            @Valid @RequestHeader("token") String token){

        TokenUser t = tokenUserRepository.findOneByToken(token).orElseThrow(BusinessException::unauthorized);
        addToCartService.checkoutOrder(checkoutRequest,t);
        return BaseResponse.createSuccessResponse("To Do Order Request");
    }
}
