package com.dzahin.dzahinshop.controller;

import com.dzahin.dzahinshop.helper.BaseConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HelloWorldController {

    @GetMapping("")
    String getProductData(){
        return "Hellooo Build:"+ BaseConstants.BUILD_NUMBER;
    }

}