package com.dzahin.dzahinshop;

import com.dzahin.dzahinshop.controller.TokenController;
import com.dzahin.dzahinshop.helper.BaseConstants;
import com.dzahin.dzahinshop.helper.BusinessException;
import com.dzahin.dzahinshop.model.response.BaseResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ErrorHandlerController {

    private final static Logger LOGGER = LogManager.getLogger(TokenController.class);

    @ExceptionHandler(value = { BusinessException.class})
    protected ResponseEntity<BaseResponse> handleConflict(BusinessException exception,WebRequest request) {
        LOGGER.error("[EXCEPTION]: ", exception);
        LOGGER.error("[REQUEST]: "+ request.getDescription(true).toString());

        BaseResponse commonResponse = BaseResponse.createFailedResponse(exception);
        return new ResponseEntity<>(commonResponse, exception.getHttpStatus());
    }

    @ExceptionHandler({
            MissingServletRequestParameterException.class,
            MethodArgumentTypeMismatchException.class,
            ConstraintViolationException.class,
            HttpMessageNotReadableException.class
    })
    public ResponseEntity<BaseResponse> handleMissingParams(Exception exception,WebRequest request) {
        LOGGER.error("[EXCEPTION]: ", exception);
        LOGGER.error("[REQUEST]: "+ request.getDescription(true).toString());

        BaseResponse commonResponse = BaseResponse.createFailedResponse(HttpStatus.BAD_REQUEST.value(),exception.getMessage());

        commonResponse.setMessage("Input Parameter Error "+
                ((exception.getClass() == HttpMessageNotReadableException.class)?BaseConstants.MISSING_JSON_BODY:null)
        );
        return new ResponseEntity<>(commonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<BaseResponse> handleMethodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest request) {
        BindingResult result = exception.getBindingResult();

        List<String> errorList = new ArrayList<>();
        result.getFieldErrors().forEach((fieldError) -> {
            errorList.add(fieldError.getField()+" : " +fieldError.getDefaultMessage() +" -> rejected value [" +fieldError.getRejectedValue() +"]" );
        });
        result.getGlobalErrors().forEach((fieldError) -> {
            errorList.add(fieldError.getObjectName()+" : " +fieldError.getDefaultMessage() );
        });

        BaseResponse commonResponse = BaseResponse.createFailedResponse(HttpStatus.BAD_REQUEST.value(),errorList);
        commonResponse.setMessage(BaseConstants.VALIDATION_ERROR);
        return new ResponseEntity<>(commonResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<BaseResponse> runTimeException(RuntimeException exception,WebRequest request) {
        LOGGER.error("[EXCEPTION]: ", exception);
        LOGGER.error("[REQUEST]: "+ request.getDescription(true).toString());

        BaseResponse commonResponse = BaseResponse.createFailedResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),exception.getMessage());
        commonResponse.setMessage("Run Time Error");
        return new ResponseEntity<>(commonResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

