package com.dzahin.dzahinshop.helper;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BusinessException extends RuntimeException {
    private String appCode;
    private String errorCode;
    private HttpStatus httpStatus;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String errorCode, String message, HttpStatus httpStatus) {
        super(message);
        this.errorCode = errorCode;
        this.httpStatus = httpStatus;
    }

    public static BusinessException unauthorized() {
        return new BusinessException("401","Unauthorized",HttpStatus.UNAUTHORIZED);
    }

    public static BusinessException notFound() {
        return new BusinessException("400","Request not found",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException invalidAPIkey() {
        return new BusinessException("401","Invalid API Key",HttpStatus.UNAUTHORIZED);
    }

    public static BusinessException productNotFound() {
        return new BusinessException("400","Product not found",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException multipleMerchantCheckout() {
        return new BusinessException("400","Multiple merchant cannot checkout at the same time",HttpStatus.BAD_REQUEST);
    }

    public static BusinessException outOfStock(String productName) {
        return new BusinessException("400",productName+" out of stock",HttpStatus.BAD_REQUEST);
    }
}
