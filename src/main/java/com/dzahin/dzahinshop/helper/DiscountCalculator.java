package com.dzahin.dzahinshop.helper;

import com.dzahin.dzahinshop.model.entity.PriceCalculate;
import com.dzahin.dzahinshop.model.entity.Product;

public class DiscountCalculator {

    public static PriceCalculate calculatePriceProduct(Product product, Integer qty){

        if(product.getFreeBonus() == null || product.getBuyCondition() == null){
            return PriceCalculate.builder()
                    .defaultPrice(product.getPrice()*qty)
                    .discountedPrice(0)
                    .actualPrice(product.getPrice()*qty)
                    .actualQty(qty)
                    .freeQty(0)
                    .build();
        }else{
            System.out.print("Beli "+qty+" Promo : "+product.getBuyCondition()+" free :"+product.getFreeBonus()+" ---> ");
            Integer remain = qty % (product.getBuyCondition()+product.getFreeBonus());
            Integer n = (qty - remain) / (product.getBuyCondition() + product.getFreeBonus());
            Integer py = Math.max(0,remain - product.getBuyCondition()) +  (n * product.getFreeBonus());
            Integer px = qty - py;
            System.out.println("Bayar "+px+" Gratis : "+py);

            return PriceCalculate.builder()
                    .defaultPrice(qty * product.getPrice())
                    .discountedPrice(py * product.getPrice())
                    .actualPrice(px* product.getPrice())
                    .actualQty(px)
                    .freeQty(py)
                    .build();
        }
    }
}
