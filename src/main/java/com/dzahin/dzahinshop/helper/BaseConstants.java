package com.dzahin.dzahinshop.helper;

public abstract class BaseConstants {

    private BaseConstants() {}

    public static final String BUILD_NUMBER = "1.0.3";
    public static final String VALIDATION_ERROR = "Validation Error";
    public static final String MISSING_JSON_BODY = "Missing JSON Body";
    public static final String API_KEY = "12345678";
}
